import numpy as np
import pdb

class TestingEnviroment:

	def __init__(self, neural_network):
		self.neural_network = neural_network

	def test(self, test_pairs, evaluation_function = lambda output_data, right_class_num : True):
		total_num = len(test_pairs)
		correct_num = 0

		for test_pair in test_pairs:
			result = self.neural_network.transform(test_pair.data)
			
			evaluation = evaluation_function(result, test_pair.class_num)

			correct_num += evaluation if 1 else 0

		return TestingResult(total_num, correct_num)

class TestingResult:

	def __init__(self, total_num, correct_num):
		self.total_num = total_num
		self.correct_num = correct_num

import numpy as np
import pdb


## This class is not written by me it was downloaded from http://www.zemris.fer.hr/~ssegvic/du/src/data.py
class Random2DGaussian:
  """Random bivariate normal distribution sampler

  Hardwired parameters:
      d0min,d0max: horizontal range for the mean
      d1min,d1max: vertical range for the mean
      scalecov: controls the covariance range 

  Methods:
      __init__: creates a new distribution

      get_sample(n): samples n datapoints

  """

  d0min=0 
  d0max=10
  d1min=0 
  d1max=10
  scalecov=5
  
  def __init__(self):
    dw0,dw1 = self.d0max-self.d0min, self.d1max-self.d1min
    mean = (self.d0min,self.d1min)
    mean += np.random.random_sample(2)*(dw0, dw1)
    eigvals = np.random.random_sample(2)
    eigvals *= (dw0/self.scalecov, dw1/self.scalecov)
    eigvals **= 2
    theta = np.random.random_sample()*np.pi*2
    R = [[np.cos(theta), -np.sin(theta)], 
         [np.sin(theta), np.cos(theta)]]
    Sigma = np.dot(np.dot(np.transpose(R), np.diag(eigvals)), R)
    self.get_sample = lambda n: np.random.multivariate_normal(mean,Sigma,n)

## This function is not written by me it was downloaded from http://www.zemris.fer.hr/~ssegvic/du/src/data.py
def sample_gmm_2d(ncomponents, nclasses, nsamples):
  # create the distributions and groundtruth labels
  Gs=[]
  Ys=[]

  for i in range(ncomponents):
    Gs.append(Random2DGaussian())
    Ys.append(i)

  # sample the dataset
  X = np.vstack([G.get_sample(nsamples) for G in Gs])
  Y_= np.hstack([[Y]*nsamples for Y in Ys])
  
  return [DataClassPair(pair[0], pair[1]) for pair in list(zip(X, Y_))]

## Class encapsulates one pair of data and classNum
## data is the vector of data that is pulled through NeuralNetwork
## classNum is the number of the class that this vector is assocciated to 
class DataClassPair:

	def __init__(self, data, class_num):
		self.data = data
		self.class_num = class_num

	def __repr__(self):
		return "{} -> {}\n".format(str(self.data), str(self.class_num))

import numpy as np
import pdb

from datetime import datetime

import samples
import core
import database
import learning
import testing
import visualisation

import pickle
import os
import functools

## Function splits array into num number of chunks
def chunkIt(seq, num):
  avg = len(seq) / float(num)
  out = []
  last = 0.0
  
  while last < len(seq):
    out.append(seq[int(last):int(last + avg)])
    last += avg

  return out

def custom_epoch_cost_consume_function(epoch_cost):
  print("Cost: {0:3.10f}".format(epoch_cost))

def custom_evaluation_function(output_data, right_class_num):
  return output_data[right_class_num] > 0.7

if __name__ == "__main__":
  ## Init the pseudo-random generator
  np.random.seed(1)

  ## "purely ranodom seed"
  ## np.random.seed(int(datetime.timestamp(datetime.now())))


  ## Data generation ##

  batch = samples.sample_gmm_2d(2, 2, 10_000)
  np.random.shuffle(batch)

  chunks = chunkIt(batch, 50);
  batches = chunks[0:48]
  test_batch = functools.reduce(lambda c, x: c + x, chunks[48:49], [])



  ## Create neural network ##

  ## define neural network layers
  nn1 = core.NeuralNetworkLayer(6, 2, function = learning.ReLU, init_element_weight = 10.0, init_element_bias = 0.0)
  nn2 = core.NeuralNetworkLayer(2, 6, function = learning.stable_softmax, init_element_weight = 11.0, init_element_bias = 0.0)
  
  neuralNetwork = core.NeuralNetwork("test1", [nn1, nn2])



  ## Learning ##

  ## Create learning enviroment for the neural network
  learningEnviroment = learning.LearningEnviroment(neuralNetwork, 0.1)

  ## Get learning results
  print("## Learning ##")
  for i in range(10):
    print(f"{i}. time repeat")
    learned_neural_network = learningEnviroment.learn(batches, epoch_cost_consume_function = custom_epoch_cost_consume_function)




  ## Testing ##

  print("## Testing ##")
  print("Test batch of size {}:".format(len(test_batch)))

  testingEnviroment = testing.TestingEnviroment(neuralNetwork)
  results = []
  for i in range(11):
    evaluation_function = lambda output_data, right_class_num : output_data[right_class_num] >= float(i)/10
    test_result = testingEnviroment.test(test_batch, evaluation_function = evaluation_function)
    results.append(float(test_result.correct_num)/test_result.total_num)

  visualisation.graph_results(results)
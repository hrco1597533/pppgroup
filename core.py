import numpy as np
import pdb

## Only defined becouse of pickle because it can't serialize lambdas
def linear_function(x):
	return x

## Class encapsulates one neural network, you must give it a name and an array of layers that are instances of NeuralNetworkLayer class
## endFunction defines a function that is used on the final output from the network similar to the function parameter in NeuralNetworkLayer but here it is only used on the last result
class NeuralNetwork:

	def __init__(self, name, neural_network_layers):
		self.validate_layers(neural_network_layers)

		self.name = name
		self.neural_network_layers = neural_network_layers

	def __repr__(self):
		res = ""
		for layer in self.neural_network_layers:
			res+=str(layer)
			res+="\n\n"
		return res

	def transform(self, input):
		result = np.array(input)

		for i in range(len(self.neural_network_layers)):
			currentLayer = self.neural_network_layers[i]
			result = currentLayer.transform(result)

		return result

	def get_matrix_of_weights(self):
		matrix = [layer.get_weights() for layer in self.neural_network_layers]
		return matrix

	def get_matrix_of_biases(self):
		matrix = [layer.get_biases() for layer in self.neural_network_layers]
		return matrix

	def set_matrix_of_weights(self, matrix):
		for i in range(len(matrix)):
			self.neural_network_layers[i].weights = matrix[i]
	
	def set_matrix_of_biases(self, matrix):
		for i in range(len(matrix)):
			self.neural_network_layers[i].biases = matrix[i]

	def validate_layers(self, neural_network_layers):
		temp = neural_network_layers[0]
		for i in range(1, len(neural_network_layers)):
			if temp.n_neurons != neural_network_layers[i].n_weights:
				nnl1, num_inputs, nnl2, num_weights = temp, temp.n_neurons, neural_network_layers[i], neural_network_layers[i].n_weights
				raise Exception("Neural network layer {}\n(inputs: {}) isn't compatible with neural network layer \n{}\n(weights per neuron: {})".format(nnl1, num_inputs, nnl2, num_weights))
			temp = neural_network_layers[i]

## Class encapsulates one layer of the neural network
## For creating this object you must define how many neurons does the layer has and how many weights does each neuron has
## You can also define weights and bieases if not defined they all initialize to 1.0
## function parameter is used for transforming result e.g. S that equals W*Xt+b, so basicaly the output of the layer is further defined by this parameter by calling it and giving it the S array as parameter
class NeuralNetworkLayer:

	def __init__(self, n_neurons, n_weights, weights=[], biases=[], init_element_weight = 1.0, init_element_bias = 1.0, function = linear_function):
		self.n_neurons = n_neurons
		self.n_weights = n_weights
		self.function = function

		self.has_result = False

		self.result = [];
		self.end_result = [];

		if len(weights) != 0:
			self.weights = np.array(weights)
		else:
			self.weights = np.array([[init_element_weight]*n_weights]*n_neurons)

		if len(biases) != 0:
			self.biases = np.array(biases)
		else:
			self.biases = np.array([init_element_bias]*n_neurons)

	def __repr__(self):
		return "{} - {}".format(str(self.weights), str(self.biases))

	def transform(self, input):
		self.result = np.add(self.weights @ input.transpose(), self.biases.transpose())
		self.end_reuslt = self.function(self.result)
		self.has_result = True
		return self.end_reuslt

	def get_weights(self):
		return self.weights

	def get_biases(self):
		return self.biases

	def get_result(self):
		if not self.has_result:
			raise Exception("No data passed through this layer")
		return self.result

	def get_end_result(self):
		if not self.has_result:
			raise Exception("No data passed through this layer")
		return self.end_reuslt

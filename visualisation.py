import matplotlib.pyplot as pp
import numpy as np

def chunkIt(seq, num):
  avg = len(seq) / float(num)
  out = []
  last = 0.0
  
  while last < len(seq):
    out.append(seq[int(last):int(last + avg)])
    last += avg

  return out

def graph_results(costs):
	x = list(map(lambda i : f"{str(int(i))}%", np.linspace(0.0, 100.0, len(costs), True)))
	pp.bar(x, costs)
	pp.show()

def plot_samples(batch, class_num, size_of_samples_for_each_class):
	colors = ['r', 'g', 'b', 'c', 'm', 'y']
	simbols = ['.',',','o','v','<','>']
	used_simbols = []
	for i in range(class_num):
		for simbol in simbols:
			for color in colors:
				used_simbols.append(color+simbol)

	batches_per_class = chunkIt(list(map(lambda pair : pair.data, batch)), class_num)
	for i in range(len(batches_per_class)):
		pp.plot([row[0] for row in batches_per_class[i]], [row[1] for row in batches_per_class[i]], used_simbols[i])

	pp.show()
Ovdje ću probat opisati što bi minimalno trebalo imati da bi mogli raditi na projektu.

Pošto smo se "odlučili" za Python, minimalno što će svakom od nas trebati jest python interpreter. Ima puno verzija Python
interpretera, međutim sve verzije Python2.xx će biti "ugašene" za par mjeseci tak da bi bilo dobro da svi imamo neku verziju
koja je barem Python3.0. Osobno sam pokrenuo ovaj kod na verziji 3.6.8, al vjerujem da će raditi na bilo kojoj verziji.

Zadnja verzija Python-a je 3.8, tak da možete to skinuti ovisno o opertivnom sistemu na ovom linku: https://www.python.org/downloads/
Ukoliko radite na linux-u vrlo je vjerojatno da već postoji neka verzija Python interpretera na mašini. Kako bi provjerili
da li postoji na vašoj mašini Python interpreter probajte upisati u terminal/command prompt/power shell(ili što god):

python --version ili python3 --version

Ukoliko vam vrati neki neku verziju ak je veća od 3.0 to je ok.

Slijedeće bi trebali moći pokrenuti kod koji sam stavio u repositoriji. Klonirajte repositoriji na svoje lokalno računalo tak da u terminal
upisete:

git clone https://gitlab.com/hrco1597533/pppgroup.git

Kad ste klonirali repositorij onda udite u njega i probajte upisati:

python main.py ili python3 main.py
(ovisno o tome dal vam je prije radio python ili python3)

Trebao bi se upaliti prozor u kojemu su iscrtane nekakve točkice. Ako je to upalilo onda ste spremni.
Slobodno pregledajte kod i postavite pitanja ak vam nes ne radi, nezz ni ja kaj se sve može dogoditi
da ne radi al probat ćemo neš sredit.
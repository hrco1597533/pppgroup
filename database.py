import pickle
import glob

## This class encapsualates all functionality of saving and loading NeuralNetwork objects
## It depends on Pickle module
## It creates a directory of name that is given in the constructor in the current directory from where script is run and there is saves all serializations of instances of NeuralNetwork class with the extension '.nnet'
class NeuralNetworkDatabase:

	def __init__(self, directory):
		self.directory = directory
		
		import os

		if not os.path.exists(directory):
			os.makedirs(directory)
		
		self.neural_networks = [nn_name[nn_name.rfind('/')+1:nn_name.rfind('.')] for nn_name in glob.glob(directory+'/*.nnet')]

	def save_nnet(self, neural_network):
		self.neural_networks.append(neural_network.name)

		file = open(self.directory+'/'+neural_network.name+'.nnet', 'wb')
		pickle.dump(neural_network, file)

	def get_nnets_names(self):
		return self.neural_networks

	def load_nnet(self, name):
		file = open(self.directory+'/'+name+'.nnet', 'rb')
		return pickle.load(file)
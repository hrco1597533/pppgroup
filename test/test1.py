import core
import learning
import pdb

nnl1 = core.NeuralNetworkLayer(2, 2)
nnl2 = core.NeuralNetworkLayer(5, 2)
nnl3 = core.NeuralNetworkLayer(3, 4)

nn = core.NeuralNetwork("Test1", [nnl1, nnl2, nnl3], endFunction = learning.stable_softmax)

sample = [2, 3]

result = nn.transform([2,3])

print(result)
import numpy as np
import pdb


## This class takes the neural network and carries the learning process
## Now it only uses the logorithmic cost function and its derivations that are hardcoded in it, but can potentially be rewrote to accepts and other cost functions
class LearningEnviroment:

	def __init__(self, neural_network, hiper_epsilon):
		## neural_network - instance of core.NeuralNetwork 
		
		self.neural_network = neural_network
		self.hiper_epsilon = hiper_epsilon
		
	def calculate_epoch_derivation(self, batch):
		## evaluate_function must return object of type ResultEvaluation
		## batch - data batch that is processed in this epoch, it is an array of instances samples.DataClassPair

		num_of_layers = len(self.neural_network.neural_network_layers)
		
		Y = []

		H_array = [[] for i in range(num_of_layers+1)]

		cost = 0

		for nn_input in batch:

			H_array[0].append(nn_input.data)

			P_i = self.neural_network.transform(nn_input.data)

			cost += np.log(P_i[nn_input.class_num])
			
			Y_i = [0]*len(P_i)
			Y_i[nn_input.class_num] = 1
			Y.append(Y_i)

			for i in range(num_of_layers):
				H_array[i+1].append(self.neural_network.neural_network_layers[i].get_end_result())

		Gs = np.subtract(H_array[num_of_layers], Y)

		dL_dW = [np.array([]) for i in range(num_of_layers)]
		dL_dB = [np.array([]) for i in range(num_of_layers)]

		N = len(batch)

		dL_dW[num_of_layers-1] = (1.0/N)*(Gs.transpose() @ np.array(H_array[num_of_layers-1]))
		dL_dB[num_of_layers-1] = (1.0/N)*np.sum(Gs.transpose(), axis=1)

		previous_Gs = Gs

		for i in range(num_of_layers-2, -1, -1):
			zero_one_matrix_function = lambda x : x > 0 if x else 0
			zero_one_matrix = np.zeros(np.array(H_array[i+1]).shape)
			for j in range(len(H_array[i+1])):
				for k in range(len(H_array[i+1][j])):
					zero_one_matrix[j][k] = zero_one_matrix_function(H_array[i+1][j][k])

				current_Gs = (previous_Gs @ self.neural_network.neural_network_layers[i+1].get_weights())*zero_one_matrix

			dL_dW[i] = (1.0/N)*(current_Gs.transpose() @ np.array(H_array[i]))
			dL_dB[i] = (1.0/N)*np.sum(current_Gs.transpose(), axis = 1)

			previous_Gs = current_Gs
			
		cost = (-1.0/N)*cost

		return EpochResult(dL_dW, dL_dB, cost)

	def learn(self, batches, epoch_cost_consume_function = lambda epoch_cost : print(epoch_cost)):
		## Learning is made currently for only one layer

		for batch in batches:
			epoch_result = self.calculate_epoch_derivation(batch)

			epoch_cost_consume_function(epoch_result.cost)

			multiply_array_of_matrix_by_factor(epoch_result.dL_dW, self.hiper_epsilon)
			dW = epoch_result.dL_dW
			multiply_array_of_matrix_by_factor(epoch_result.dL_dB, self.hiper_epsilon)
			dB = epoch_result.dL_dB

			new_matrix_of_weights = []
			new_matrix_of_biases = []

			for i in range(len(self.neural_network.neural_network_layers)):
				new_matrix_of_weights.append(np.subtract(self.neural_network.get_matrix_of_weights()[i], dW[i]))
				new_matrix_of_biases.append(np.subtract(self.neural_network.get_matrix_of_biases()[i], dB[i]))
			self.neural_network.set_matrix_of_weights(new_matrix_of_weights)
			self.neural_network.set_matrix_of_biases(new_matrix_of_biases)

		return self.neural_network

class EpochResult:

	def __init__(self, dL_dW, dL_dB, cost):
		self.dL_dW = dL_dW
		self.dL_dB = dL_dB
		self.cost = cost

class LearningResult:

	def __init__(self, total_inputs_num, total_correct_num):
		self.total_inputs_num = total_inputs_num
		self.total_correct_num = total_correct_num

class ResultEvaluation:

	def __init__(self, valid, class_num):
		self.valid = valid
		self.class_num = class_num;

# stabilni softmax
def stable_softmax(x):
  exp_x_shifted = np.exp(x - np.max(x))
  probs = exp_x_shifted / np.sum(exp_x_shifted)
  return probs

def ReLU(x):
	function = lambda x : x < 0 if 0 else x
	return function(x)

def multiply_array_of_matrix_by_factor(array, factor):
	for i in range(len(array)):
		array[i] = array[i]*factor
